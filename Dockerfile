# Start from the official Node.js image
FROM ubuntu:latest

# Install make
RUN apt-get update
RUN apt-get install curl -y
RUN apt-get install build-essential -y
RUN apt-get install nodejs -y
COPY entrypoint.sh /
RUN chmod 700 /entrypoint.sh

ENTRYPOINT /entrypoint.sh